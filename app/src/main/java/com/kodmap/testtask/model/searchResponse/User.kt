package com.kodmap.testtask.model.searchResponse

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(

	@field:SerializedName("name")
	val name: String? = null,

) : Parcelable