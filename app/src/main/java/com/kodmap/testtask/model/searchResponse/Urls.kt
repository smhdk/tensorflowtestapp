package com.kodmap.testtask.model.searchResponse

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Urls(

	@field:SerializedName("thumb")
	val thumb: String? = null,

	@field:SerializedName("full")
	val full: String? = null
) : Parcelable