package com.kodmap.testtask.model.searchResponse

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ResultsItem(

	@field:SerializedName("urls")
	val urls: Urls? = null,

	@field:SerializedName("user")
	val user: User? = null,
) : Parcelable