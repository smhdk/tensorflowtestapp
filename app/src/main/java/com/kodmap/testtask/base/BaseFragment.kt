package com.kodmap.testtask.base

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<V : ViewBinding>(@LayoutRes layout: Int) : Fragment(layout),
    LifecycleObserver {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initArgs()
        initView()
    }

    abstract fun initView()

    abstract val mBinding: V

    open fun initArgs() {}

}