package com.kodmap.testtask.base

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.kodmap.testtask.R

abstract class BaseLoadStateAdapter() :
    LoadStateAdapter<BaseLoadStateAdapter.LoadStateViewHolder<ViewDataBinding>>() {

    override fun onBindViewHolder(
        holder: LoadStateViewHolder<ViewDataBinding>,
        loadState: LoadState
    ) {
        holder.binding.root.setTag(R.string.position, loadState.toString())
        bind(holder.binding, loadState)
    }

    protected abstract fun bind(binding: ViewDataBinding, loadState: LoadState)

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState) =
        getViewHolder(parent, loadState)

    open fun getViewHolder(parent: ViewGroup, loadState: LoadState) =
        LoadStateViewHolder(createBinding(parent, loadState))

    abstract fun createBinding(parent: ViewGroup, loadState: LoadState): ViewDataBinding

    open class LoadStateViewHolder<out T : ViewDataBinding>(val binding: T) :
        RecyclerView.ViewHolder(binding.root)
}