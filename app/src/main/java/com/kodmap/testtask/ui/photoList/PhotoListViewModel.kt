package com.kodmap.testtask.ui.photoList

import androidx.lifecycle.*
import com.kodmap.testtask.repo.SearchRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PhotoListViewModel @Inject constructor(private val searchRepo: SearchRepo) : ViewModel() {

    val query = MutableLiveData("")

    val searchList = query.switchMap {
        searchRepo.getSearchResult(it, viewModelScope).asLiveData()
    }
}