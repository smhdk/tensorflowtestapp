package com.kodmap.testtask.ui.main

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import androidx.navigation.ui.setupWithNavController
import com.kodmap.testtask.R
import com.kodmap.testtask.base.BaseActivity
import com.kodmap.testtask.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {
    override val layoutRes: Int
        get() = R.layout.activity_main

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initToolbar()
        initNavController()
        initDestinationChangeListener()
    }

    private fun initDestinationChangeListener() {
        findNavController(R.id.nav_host_fragment_content_main)
            .addOnDestinationChangedListener { controller, destination, arguments ->
                if (destination.id == R.id.navigation_photo_list) {
                    mBinding.toolbar.title = "${arguments?.get("name").toString()} Photos"
                }
            }
    }

    private fun initNavController() {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
        mBinding.bnvMain.setupWithNavController(navController)

    }

    private fun initToolbar() {
        setSupportActionBar(mBinding.toolbar)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

}