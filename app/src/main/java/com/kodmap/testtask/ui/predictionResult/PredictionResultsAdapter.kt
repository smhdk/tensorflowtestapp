package com.kodmap.testtask.ui.predictionResult

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.kodmap.testtask.R
import com.kodmap.testtask.base.BasePagedListAdapter
import com.kodmap.testtask.databinding.ItemPredictionResultBinding
import com.kodmap.testtask.db.entity.PredictionResultEntity
import com.kodmap.testtask.utils.ext.bindingInflate

class PredictionResultsAdapter(
    private val callBack: (PredictionResultEntity?) -> Unit
) :
    BasePagedListAdapter<PredictionResultEntity>(diffCallback) {

    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        val mBinding =
            parent.bindingInflate<ItemPredictionResultBinding>(R.layout.item_prediction_result)
        mBinding.root.setOnClickListener {
            callBack.invoke(mBinding.item)
        }
        return mBinding
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        (binding as ItemPredictionResultBinding).item = getItem(position)
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<PredictionResultEntity>() {
            override fun areItemsTheSame(
                oldItem: PredictionResultEntity,
                newItem: PredictionResultEntity
            ) =
                newItem.id == oldItem.id

            override fun areContentsTheSame(
                oldItem: PredictionResultEntity,
                newItem: PredictionResultEntity
            ) = newItem == oldItem
        }
    }
}