package com.kodmap.testtask.ui.photoList

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.kodmap.testtask.R
import com.kodmap.testtask.base.BaseFragment
import com.kodmap.testtask.databinding.FragmentPhotoListBinding
import com.kodmap.testtask.utils.LinkUtils
import com.kodmap.testtask.utils.ext.setVisibilityForLoadState
import com.kodmap.testtask.utils.ext.viewBinding
import com.kodmap.testtask.utils.ui.ItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PhotoListFragment @Inject constructor() :
    BaseFragment<FragmentPhotoListBinding>(R.layout.fragment_photo_list) {

    override val mBinding by viewBinding(FragmentPhotoListBinding::bind)
    private val viewModel: PhotoListViewModel by viewModels()
    private val args: PhotoListFragmentArgs by navArgs()

    private val photoListAdapter by lazy {
        PhotoListAdapter {
            LinkUtils.openLink(requireContext(),it?.urls?.full)
        }
    }

    override fun initView() {
        mBinding.viewModel = viewModel
        mBinding.setLifecycleOwner { viewLifecycleOwner.lifecycle }

        initAdapter()
        observePhotoList()
    }

    override fun initArgs() {
        super.initArgs()
        viewModel.query.value = args.name
    }

    private fun observePhotoList() {
        viewModel.searchList.observe(viewLifecycleOwner, {
            viewLifecycleOwner.lifecycleScope.launch {
                photoListAdapter.submitData(it)
            }
        })
    }

    private fun initAdapter() {
        with(mBinding.rvPhotoList) {
            this.layoutManager = GridLayoutManager(
                requireContext(),
                2
            )
            this.addItemDecoration(ItemDecoration(8))
            this.adapter = photoListAdapter.withLoadStateHeaderAndFooter(
                PhotoListLoadStateAdapter(),
                PhotoListLoadStateAdapter()
            )

            viewLifecycleOwner.lifecycleScope.launch {
                photoListAdapter.loadStateFlow.collect {
                    mBinding.viewLoading.setVisibilityForLoadState(it)
                }
            }
        }
    }
}