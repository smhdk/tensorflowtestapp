package com.kodmap.testtask.ui.photoList

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.kodmap.testtask.R
import com.kodmap.testtask.base.BasePagedListAdapter
import com.kodmap.testtask.databinding.ItemPhotoBinding
import com.kodmap.testtask.model.searchResponse.ResultsItem
import com.kodmap.testtask.utils.ext.bindingInflate

class PhotoListAdapter(
    private val callBack: (ResultsItem?) -> Unit
) :
    BasePagedListAdapter<ResultsItem>(diffCallback) {

    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        val mBinding = parent.bindingInflate<ItemPhotoBinding>(R.layout.item_photo)
        mBinding.root.setOnClickListener {
            callBack.invoke(mBinding.item)
        }
        return mBinding
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        (binding as ItemPhotoBinding).item = getItem(position)
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ResultsItem>() {
            override fun areItemsTheSame(oldItem: ResultsItem, newItem: ResultsItem) =
                newItem == oldItem

            override fun areContentsTheSame(
                oldItem: ResultsItem,
                newItem: ResultsItem
            ) = newItem == oldItem
        }
    }
}