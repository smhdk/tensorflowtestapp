package com.kodmap.testtask.ui.predictionResult

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.kodmap.testtask.R
import com.kodmap.testtask.base.BaseFragment
import com.kodmap.testtask.databinding.FragmentPredictionResultsBinding
import com.kodmap.testtask.utils.ext.viewBinding
import com.kodmap.testtask.utils.ui.ItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PredictionResultsFragment @Inject constructor() :
    BaseFragment<FragmentPredictionResultsBinding>(R.layout.fragment_prediction_results) {

    override val mBinding by viewBinding(FragmentPredictionResultsBinding::bind)
    private val viewModel: PredictionResultsViewModel by viewModels()

    private val predictionResultsAdapter by lazy {
        PredictionResultsAdapter {
            findNavController().navigate(
                R.id.action_PredictionResults_to_PhotoListFragment,
                bundleOf("name" to it?.name)
            )
        }
    }
    private var menu: Menu? = null

    override fun initView() {
        mBinding.viewModel = viewModel
        mBinding.setLifecycleOwner { viewLifecycleOwner.lifecycle }

        initAdapter()
        observePhotoList()

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_prediction_result, menu)
        this.menu = menu
        menu.findItem(R.id.item_delete)?.isVisible = predictionResultsAdapter.itemCount > 1
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_delete -> {
                viewModel.deletePredictionResults()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun observePhotoList() {
        viewModel.getPredictionResults.observe(viewLifecycleOwner, {
            viewLifecycleOwner.lifecycleScope.launch {
                predictionResultsAdapter.submitData(it)
            }
        })
    }

    private fun initAdapter() {
        with(mBinding.rvPredictionResults) {
            this.adapter = predictionResultsAdapter
            this.addItemDecoration(ItemDecoration(8))

            viewLifecycleOwner.lifecycleScope.launch {
                predictionResultsAdapter.loadStateFlow.collect {
                    if (it.append is LoadState.NotLoading && it.append.endOfPaginationReached) {
                        mBinding.viewEmpty.isVisible = predictionResultsAdapter.itemCount < 1
                        menu?.findItem(R.id.item_delete)?.isVisible = predictionResultsAdapter.itemCount > 1
                    }
                }
            }
        }
    }
}