package com.kodmap.testtask.ui.camera

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.RectF
import android.net.Uri
import android.os.CountDownTimer
import android.provider.Settings
import android.util.Log
import android.util.Size
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.AspectRatio
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.kodmap.testtask.BuildConfig
import com.kodmap.testtask.R
import com.kodmap.testtask.base.BaseFragment
import com.kodmap.testtask.databinding.FragmentCameraBinding
import com.kodmap.testtask.db.entity.PredictionResultEntity
import com.kodmap.testtask.utils.PermissionHelper
import com.kodmap.testtask.utils.camera.ObjectDetectionHelper
import com.kodmap.testtask.utils.camera.YuvToRgbConverter
import com.kodmap.testtask.utils.ext.hide
import com.kodmap.testtask.utils.ext.show
import com.kodmap.testtask.utils.ext.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import org.tensorflow.lite.DataType
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.nnapi.NnApiDelegate
import org.tensorflow.lite.support.common.FileUtil
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.image.ops.ResizeWithCropOrPadOp
import org.tensorflow.lite.support.image.ops.Rot90Op
import java.util.concurrent.Executors
import javax.inject.Inject
import kotlin.math.min

@AndroidEntryPoint
class CameraFragment @Inject constructor() :
    BaseFragment<FragmentCameraBinding>(R.layout.fragment_camera) {

    private var predictionTimer: CountDownTimer? = null

    private lateinit var bitmapBuffer: Bitmap

    private val executor = Executors.newSingleThreadExecutor()

    private var lensFacing: Int = CameraSelector.LENS_FACING_BACK

    private var pauseAnalysis = false
    private var imageRotationDegrees: Int = 0
    private val tfImageBuffer = TensorImage(DataType.UINT8)

    private val tfImageProcessor by lazy {
        val cropSize = minOf(bitmapBuffer.width, bitmapBuffer.height)
        ImageProcessor.Builder()
            .add(ResizeWithCropOrPadOp(cropSize, cropSize))
            .add(
                ResizeOp(
                    tfInputSize.height, tfInputSize.width, ResizeOp.ResizeMethod.NEAREST_NEIGHBOR
                )
            )
            .add(Rot90Op(imageRotationDegrees / 90))
            .add(NormalizeOp(0f, 1f))
            .build()
    }

    private val tflite by lazy {
        Interpreter(
            FileUtil.loadMappedFile(requireContext(), MODEL_PATH),
            Interpreter.Options().addDelegate(NnApiDelegate())
        )
    }

    private val detector by lazy {
        ObjectDetectionHelper(tflite, FileUtil.loadLabels(requireContext(), LABELS_PATH))
    }

    private val tfInputSize by lazy {
        val inputIndex = 0
        val inputShape = tflite.getInputTensor(inputIndex).shape()
        Size(inputShape[2], inputShape[1])
    }

    override val mBinding by viewBinding(FragmentCameraBinding::bind)
    private val viewModel: CameraViewModel by viewModels()

    override fun initView() {
        observePredictionResult()
        initClickListeners()
    }

    private fun initClickListeners() {
        mBinding.btnName.setOnClickListener {
            onNameClick()
        }
        mBinding.btnAllowCameraPermission.setOnClickListener {
            onAllowCameraPermssionClick()
        }
    }

    private fun onNameClick() {
        findNavController().navigate(
            R.id.action_CameraFragment_to_PhotoListFragment,
            bundleOf("name" to mBinding.btnName.text.toString())
        )
    }

    private fun onAllowCameraPermssionClick() {

        if (PermissionHelper.isPermissionNeverAsk(
                requireActivity(),
                Manifest.permission.CAMERA
            )
        ) {
            startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                data = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
            })
        } else {
            requestCameraPermission()
        }
    }

    companion object {
        private const val ACCURACY_THRESHOLD = 0.5f
        private const val MODEL_PATH = "metadata.tflite"
        private const val LABELS_PATH = "labels.txt"
    }

    private val cameraPermissionResultCallback =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                mBinding.btnAllowCameraPermission.hide()
                bindCameraUseCases()
            }
        }

    private fun requestCameraPermission() {
        cameraPermissionResultCallback.launch(Manifest.permission.CAMERA)
    }

    override fun onResume() {
        super.onResume()
        if (!PermissionHelper.isPermissionGranted(requireContext(), Manifest.permission.CAMERA)
            && !PermissionHelper.isPermissionNeverAsk(requireActivity(), Manifest.permission.CAMERA)
        ) {
            requestCameraPermission()
        } else if (PermissionHelper.isPermissionGranted(
                requireContext(),
                Manifest.permission.CAMERA
            )
        ) {
            pauseAnalysis = false
            mBinding.btnAllowCameraPermission.hide()
            bindCameraUseCases()
        } else if (!PermissionHelper.isPermissionGranted(
                requireContext(),
                Manifest.permission.CAMERA
            )
        ) {
            mBinding.btnAllowCameraPermission.show()
        }
    }

    @SuppressLint("UnsafeOptInUsageError")
    private fun bindCameraUseCases() = mBinding.previewView.post {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()

            val preview = Preview.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                .setTargetRotation(mBinding.previewView.display.rotation)
                .build()

            val imageAnalysis = ImageAnalysis.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                .setTargetRotation(mBinding.previewView.display.rotation)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()

            var frameCounter = 0
            var lastFpsTimestamp = System.currentTimeMillis()
            val converter = YuvToRgbConverter(requireContext())

            imageAnalysis.setAnalyzer(executor, ImageAnalysis.Analyzer { image ->
                if (!::bitmapBuffer.isInitialized) {
                    imageRotationDegrees = image.imageInfo.rotationDegrees
                    bitmapBuffer = Bitmap.createBitmap(
                        image.width, image.height, Bitmap.Config.ARGB_8888
                    )
                }

                if (pauseAnalysis) {
                    image.close()
                    return@Analyzer
                }

                image.use { converter.yuvToRgb(image.image!!, bitmapBuffer) }

                val tfImage = tfImageProcessor.process(tfImageBuffer.apply { load(bitmapBuffer) })

                val predictions = detector.predict(tfImage)

                viewModel.predictionResult.postValue(predictions.maxByOrNull { it.score })

                val frameCount = 10
                if (++frameCounter % frameCount == 0) {
                    frameCounter = 0
                    val now = System.currentTimeMillis()
                    val delta = now - lastFpsTimestamp
                    val fps = 1000 * frameCount.toFloat() / delta
                    Log.d("TAG", "FPS: ${"%.02f".format(fps)}")
                    lastFpsTimestamp = now
                }
            })

            val cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()

            cameraProvider.unbindAll()
            cameraProvider.bindToLifecycle(
                viewLifecycleOwner, cameraSelector, preview, imageAnalysis
            )

            preview.setSurfaceProvider(mBinding.previewView.surfaceProvider)

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun observePredictionResult() {
        viewModel.predictionResult.observe(viewLifecycleOwner, Observer { prediction ->
            if (prediction == null || prediction.score < ACCURACY_THRESHOLD) {
                mBinding.tvScore.hide()
                mBinding.viewBoxPrediction.hide()
                return@Observer
            }

            val location = mapOutputCoordinates(prediction.location)
            (mBinding.viewBoxPrediction.layoutParams as ViewGroup.MarginLayoutParams).apply {
                topMargin = location.top.toInt()
                leftMargin = location.left.toInt()
                width =
                    min(mBinding.previewView.width, location.right.toInt() - location.left.toInt())
                height =
                    min(mBinding.previewView.height, location.bottom.toInt() - location.top.toInt())
            }
            mBinding.viewBoxPrediction.requestLayout()

            mBinding.tvScore.show()
            mBinding.viewBoxPrediction.show()
            mBinding.btnName.show()

            mBinding.tvScore.text = "${"%.2f".format(prediction.score)}"
            mBinding.btnName.text = prediction.label

            viewModel.insertPredictionResult(
                PredictionResultEntity(
                    name = prediction.label,
                    score = prediction.score,
                    dateInMilis = System.currentTimeMillis()
                )
            )

            startLayoutPurchaseTimer()
        })
    }

    private fun startLayoutPurchaseTimer() {
        predictionTimer?.cancel()
        predictionTimer = object : CountDownTimer(3000, 100) {
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                mBinding.btnName.hide()
            }
        }
        predictionTimer?.start()
    }

    override fun onPause() {
        super.onPause()
        pauseAnalysis = true
        predictionTimer?.cancel()
        viewModel.predictionResult.value = null
    }

    private fun mapOutputCoordinates(location: RectF): RectF {

        // Step 1: map location to the preview coordinates
        val previewLocation = RectF(
            location.left * mBinding.previewView.width,
            location.top * mBinding.previewView.height,
            location.right * mBinding.previewView.width,
            location.bottom * mBinding.previewView.height
        )

        val isFrontFacing = lensFacing == CameraSelector.LENS_FACING_FRONT
        val isFlippedOrientation = imageRotationDegrees == 90 || imageRotationDegrees == 270
        val rotatedLocation = if (
            (!isFrontFacing && isFlippedOrientation) ||
            (isFrontFacing && !isFlippedOrientation)
        ) {
            RectF(
                mBinding.previewView.width - previewLocation.right,
                mBinding.previewView.height - previewLocation.bottom,
                mBinding.previewView.width - previewLocation.left,
                mBinding.previewView.height - previewLocation.top
            )
        } else {
            previewLocation
        }

        val margin = 0.1f
        val requestedRatio = 4f / 3f
        val midX = (rotatedLocation.left + rotatedLocation.right) / 2f
        val midY = (rotatedLocation.top + rotatedLocation.bottom) / 2f
        return if (mBinding.previewView.width < mBinding.previewView.height) {
            RectF(
                midX - (1f + margin) * requestedRatio * rotatedLocation.width() / 2f,
                midY - (1f - margin) * rotatedLocation.height() / 2f,
                midX + (1f + margin) * requestedRatio * rotatedLocation.width() / 2f,
                midY + (1f - margin) * rotatedLocation.height() / 2f
            )
        } else {
            RectF(
                midX - (1f - margin) * rotatedLocation.width() / 2f,
                midY - (1f + margin) * requestedRatio * rotatedLocation.height() / 2f,
                midX + (1f - margin) * rotatedLocation.width() / 2f,
                midY + (1f + margin) * requestedRatio * rotatedLocation.height() / 2f
            )
        }
    }

}