package com.kodmap.testtask.ui.photoList

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.LoadState
import com.kodmap.testtask.R
import com.kodmap.testtask.base.BaseLoadStateAdapter
import com.kodmap.testtask.databinding.ItemPhotoLoadingBinding
import com.kodmap.testtask.utils.ext.bindingInflate

class PhotoListLoadStateAdapter : BaseLoadStateAdapter() {
    override fun bind(binding: ViewDataBinding, loadState: LoadState) {
        (binding as ItemPhotoLoadingBinding).item = loadState
    }

    override fun createBinding(parent: ViewGroup, loadState: LoadState): ViewDataBinding {
        return parent.bindingInflate<ItemPhotoLoadingBinding>(R.layout.item_photo_loading)
    }
}