package com.kodmap.testtask.ui.predictionResult

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.kodmap.testtask.repo.PredictionRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PredictionResultsViewModel @Inject constructor(private val predictionRepo: PredictionRepo) :
    ViewModel() {

    val getPredictionResults = predictionRepo.getLivePredictionResults(viewModelScope).asLiveData()

    fun deletePredictionResults() {
        predictionRepo.deletePredictionResults()
    }
}