package com.kodmap.testtask.ui.camera

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kodmap.testtask.db.entity.PredictionResultEntity
import com.kodmap.testtask.repo.PredictionRepo
import com.kodmap.testtask.utils.camera.ObjectDetectionHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CameraViewModel @Inject constructor(private val predictionRepo: PredictionRepo) :
    ViewModel() {
    var predictionResult = MutableLiveData<ObjectDetectionHelper.ObjectPrediction?>()

    fun insertPredictionResult(predictionResultEntity: PredictionResultEntity) {
        predictionRepo.insertPredictionResult(predictionResultEntity)
    }
}