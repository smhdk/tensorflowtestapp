package com.kodmap.testtask.utils.ui

import android.R.attr
import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager




class ItemDecoration(private val spanSize: Int? = null) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (parent.layoutManager is GridLayoutManager) {
            val layoutManager = parent.layoutManager as GridLayoutManager
            val spanCount = layoutManager.spanCount
            val spacing = if (spanSize != null) dpToPx(spanSize) else when (spanCount) {
                1 -> {
                    dpToPx(8)
                }
                2 -> {
                    dpToPx(8)
                }
                3 -> {
                    dpToPx(4)
                }
                else -> {
                    dpToPx(4)
                }
            }
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column


            outRect.left =
                spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
            outRect.right =
                (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

            if (position < spanCount) { // top edge
                outRect.top = spacing
            }
            outRect.bottom = spacing // item bottom
        } else if (parent.layoutManager is LinearLayoutManager) {
            val spacing =
                if (spanSize != null) dpToPx(spanSize) else dpToPx(8)
            if ((parent.layoutManager as LinearLayoutManager).orientation == LinearLayoutManager.VERTICAL) {
                outRect.set(spacing, 0, spacing, spacing)
                if (parent.getChildAdapterPosition(view) == 0) {
                    outRect.top = spacing
                }
            } else if ((parent.layoutManager as LinearLayoutManager).orientation == LinearLayoutManager.HORIZONTAL) {
                outRect.set(spacing, 0, spacing, 0)
            }
        }
    }

    private fun dpToPx(dp: Int?): Int {
        return ((dp ?: 1) * Resources.getSystem().displayMetrics.density).toInt()
    }
}