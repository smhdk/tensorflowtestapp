package com.kodmap.testtask.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object PermissionHelper {
    fun isPermissionGranted(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun isPermissionNeverAsk(context: Activity, permission: String): Boolean {
        return !ActivityCompat.shouldShowRequestPermissionRationale(
            context,
            permission
        )
    }
}