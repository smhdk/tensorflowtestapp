package com.kodmap.testtask.utils

import android.content.Context
import android.content.Intent
import android.net.Uri

object LinkUtils {
    fun openLink(context: Context, link: String?) {
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(link)
                )
            )
        } catch (e: Exception) {
        }
    }
}