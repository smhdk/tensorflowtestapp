package com.kodmap.testtask.utils.ext

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState

fun <T : ViewDataBinding?> ViewGroup.bindingInflate(@LayoutRes resourceId: Int) =
    DataBindingUtil.inflate<T>(
        LayoutInflater.from(context),
        resourceId,
        this,
        false
    )

fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View?.setVisibilityForLoadState(state: CombinedLoadStates) {
    when {
        state.refresh is LoadState.NotLoading -> {
            this?.hide()
        }
        state.refresh is LoadState.Loading && !state.prepend.endOfPaginationReached && !state.append.endOfPaginationReached -> {
            this?.show()
        }
    }
}