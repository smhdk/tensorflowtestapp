package com.kodmap.testtask.utils.ext

import com.kodmap.testtask.core.Resource

suspend fun <T> getResult(call: suspend () -> T): Resource<T> {
    try {
        val response = call.invoke()
        if (response != null) {
            return if (response is List<*> && response.isEmpty()) {
                Resource.empty(response)
            } else {
                Resource.success(response)
            }
        }
        return Resource.error("")
    } catch (e: Exception) {
        return Resource.error(e.message ?: e.toString())
    }
}