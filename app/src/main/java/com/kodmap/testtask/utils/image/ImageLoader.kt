package com.kodmap.testtask.utils.image

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kodmap.testtask.R

object ImageLoader {

    fun setImage(imageView: ImageView, url: String?) {
        val imagePlaceHolder = R.drawable.bg_rectangle_transparant_solid
        Glide
            .with(imageView.context)
            .load(url)
            .thumbnail(0.5f)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(imagePlaceHolder)
            .error(imagePlaceHolder)
            .into(imageView)
    }
}