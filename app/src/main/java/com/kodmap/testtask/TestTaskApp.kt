package com.kodmap.testtask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestTaskApp : Application() {

}