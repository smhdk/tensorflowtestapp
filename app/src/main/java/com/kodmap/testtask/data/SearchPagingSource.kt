package com.kodmap.testtask.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.kodmap.testtask.api.UnsplashApi
import com.kodmap.testtask.core.Status
import com.kodmap.testtask.model.searchResponse.ResultsItem
import com.kodmap.testtask.utils.ext.getResult

class SearchPagingSource constructor(
    private val query: String?,
    private val unsplashApi: UnsplashApi
) : PagingSource<Int, ResultsItem>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ResultsItem> {
        val position = params.key ?: STARTING_INDEX

        return try {
            val response =
                getResult { unsplashApi.getSearchList(query, position) }
            when (response.status) {
                Status.SUCCESS, Status.EMPTY -> {
                    LoadResult.Page(
                        data = response.data?.results ?: emptyList(),
                        prevKey = if (position == STARTING_INDEX) null else position - 1,
                        nextKey = if (response.data?.results?.isNullOrEmpty() == true) null else position + 1
                    )
                }
                else -> {
                    LoadResult.Error(Throwable())
                }
            }

        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    companion object {
        const val STARTING_INDEX = 1
    }

    override fun getRefreshKey(state: PagingState<Int, ResultsItem>): Int {
        return 1
    }
}
