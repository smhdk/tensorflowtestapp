package com.kodmap.testtask.core

import com.kodmap.testtask.api.UnsplashApi

class Constants {
    object AppConstants{
        const val UnsplashApiBaseUrl = "https://api.unsplash.com/"
        const val PageSize = 10
    }
}