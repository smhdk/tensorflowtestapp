package com.kodmap.testtask.core

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.kodmap.testtask.utils.image.ImageLoader

object BindingAdapter {
    @JvmStatic
    @BindingAdapter("setImage")
    fun bindSetImage(
        imageView: ImageView,
        url: String?
    ) {
        ImageLoader.setImage(imageView, url)
    }

}