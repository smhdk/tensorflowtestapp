package com.kodmap.testtask.core

data class Resource<out T>(
    val status: Status,
    val data: T? = null,
    val message: String? = null,
    val errorCode: Int? = null
) {
    companion object {

        fun <T> success(data: T): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String?, errorCode: Int? = null, data: T? = null): Resource<T> {
            return Resource(Status.ERROR, data, message, errorCode)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

        fun <T> empty(data: T? = null): Resource<T> {
            return Resource(Status.EMPTY, data, null)
        }
    }
}
