package com.kodmap.testtask.core

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
    EMPTY
}