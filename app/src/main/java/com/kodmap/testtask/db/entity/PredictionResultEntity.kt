package com.kodmap.testtask.db.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
@Entity(tableName = "PredictionResult")
data class PredictionResultEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,

    var name: String? = "",

    var score: Float? = 0f,

    var dateInMilis: Long? = 0L
) : Parcelable {
    fun getFormattedScore() = "${"%.2f".format(score)}"

    fun getFormattedDate(): String {
        val date = Date()
        date.time = dateInMilis ?: System.currentTimeMillis()
        return SimpleDateFormat("dd MMM yyyy / HH:mm:ss", Locale.getDefault()).format(date)
    }
}