package com.kodmap.testtask.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kodmap.testtask.db.dao.PredictionResultDao
import com.kodmap.testtask.db.entity.PredictionResultEntity

@Database(
    entities = [
        PredictionResultEntity::class], version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun predictionResultDao(): PredictionResultDao
}