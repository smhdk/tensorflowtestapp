package com.kodmap.testtask.db.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.kodmap.testtask.db.entity.PredictionResultEntity

@Dao
interface PredictionResultDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPredictionResult(predictionResult: PredictionResultEntity?)

    @Query("Delete from PredictionResult")
    fun deletePredictionResults()

    @Query("Select * from PredictionResult order by dateInMilis desc")
    fun getLivePredictionResultList(): PagingSource<Int, PredictionResultEntity>

}