package com.kodmap.testtask.api

import com.kodmap.testtask.model.searchResponse.SearchResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface UnsplashApi {
    @GET("/search/photos")
    suspend fun getSearchList(
        @Query("query") query: String? = null,
        @Query("page") page: Int?,
    ): SearchResponse
}