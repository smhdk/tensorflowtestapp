package com.kodmap.testtask.repo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.kodmap.testtask.api.UnsplashApi
import com.kodmap.testtask.core.Constants
import com.kodmap.testtask.db.dao.PredictionResultDao
import com.kodmap.testtask.db.entity.PredictionResultEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

class PredictionRepo @Inject constructor(
    private val predictionResultDao: PredictionResultDao
) {
    fun insertPredictionResult(predictionResult: PredictionResultEntity) {
        GlobalScope.launch {
            predictionResultDao.insertPredictionResult(predictionResult)
        }
    }

    fun getLivePredictionResults(viewModelScope: CoroutineScope): Flow<PagingData<PredictionResultEntity>> {
        return Pager(
            config = PagingConfig(
                pageSize = Constants.AppConstants.PageSize,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { predictionResultDao.getLivePredictionResultList() }
        ).flow
            .cachedIn(viewModelScope)
    }

    fun deletePredictionResults() {
        GlobalScope.launch {
            predictionResultDao.deletePredictionResults()
        }
    }
}