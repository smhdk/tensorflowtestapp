package com.kodmap.testtask.repo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.kodmap.testtask.api.UnsplashApi
import com.kodmap.testtask.core.Constants
import com.kodmap.testtask.data.SearchPagingSource
import com.kodmap.testtask.model.searchResponse.ResultsItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SearchRepo @Inject constructor(
    private val unsplashApi: UnsplashApi
) {
    fun getSearchResult(
        query: String?,
        viewModelScope: CoroutineScope
    ): Flow<PagingData<ResultsItem>> {
        return Pager(
            config = PagingConfig(
                pageSize = Constants.AppConstants.PageSize,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                SearchPagingSource(
                    query,
                    unsplashApi
                )
            }
        )
            .flow
            .cachedIn(viewModelScope)
    }
}